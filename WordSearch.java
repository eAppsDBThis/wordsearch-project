import java.io.*;
import java.io.File;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.text.*;
import java.util.ArrayList;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

public class WordSearch

{

  public Integer                   charsLeftInCol;
  public Integer                   charsLeftInRow;
  public Integer                   puzzleWordCol;
  public Integer                   puzzleWordRow;
  public Integer                   requestWordCol;
  public Integer                   requestWordLength;
  public Integer                   requestWordRow;
  public boolean                   continueOK;
  public boolean                   searchWordFoundOK;
  public static	int                colNbr;
  public static	int                rowNbr;
  public static ArrayList<String>  requestWords  = new ArrayList<String>();
  public static BufferedReader     thisInputFile;
  public static Integer            puzzleCols;
  public static Integer            puzzleRows;
  public static Integer            requestWordRows;
  public static Integer            wordPuzzleRows;
  public static String             charFound;
  public static String             inputFileName;
  public static String             requestWordsString;
  public static String             startPos;
  public static String             stopPos;
  public static String             wordMatrix;
  public static boolean            isBreakOK;
  public static char               requestWordsChar;
  public static char[][]           puzzleChars;
  public static char[][]           puzzleWords;

  public BufferedWriter openTextOutPut (String pFileSpecs,
                                        String pOpenMode)
                        throws Exception

  {

    if (pOpenMode.equals("A"))
       {
         BufferedWriter thisFile = new BufferedWriter(new FileWriter(pFileSpecs, true));
         return thisFile;
	   }
     else
       {
         BufferedWriter thisFile = new BufferedWriter(new FileWriter(pFileSpecs));
         return thisFile;
       }

  } // openTextOutPut

  public BufferedReader openTextInPut (String pFileSpecs)
                        throws Exception

  {

    BufferedReader thisFile = new BufferedReader(new FileReader(pFileSpecs));

    return thisFile;

  } // openTextInPut

  public void closeTextOutPut (BufferedWriter pFileSpecs)
              throws Exception

  {

    pFileSpecs.close();

  } // closeTextOutPut

  public void closeTextInPut (BufferedReader pFileSpecs)
              throws Exception

  {

    pFileSpecs.close();

  } // closeTextInPut

  public String readTextRec (BufferedReader pThisFile)
                throws Exception

  {

    String vInPutStr;

    vInPutStr = pThisFile.readLine();

    return vInPutStr;

  } //readTextRec

  public char[][] getPuzzleWords ()
                throws IOException, Exception

  {

    String thisString;

	char[][] pChars = new char[puzzleRows +1][puzzleCols +1];

    thisInputFile = openTextInPut ("U:\\myJavaStuff\\myJava\\JavaExec\\WordFile.txt");

    rowNbr = 0;

    while ((thisString = thisInputFile.readLine()) != null)
     {
		colNbr = 0;

		String [] letters = thisString.split(" ");
		for (String thisChar : letters)
		{
		  pChars[rowNbr][colNbr] = Character.toUpperCase(thisChar.charAt(0));
		  colNbr++;
		}
		rowNbr++;

	}

    closeTextInPut(thisInputFile);

    return pChars;

  } // getPuzzleWords

  public ArrayList<String> getRequestWords ()
                throws IOException, Exception

  {

    ArrayList<String>  theseWords = new ArrayList<String>();
    String thisString;

    requestWordRows = 0;

    thisInputFile = openTextInPut ("U:\\myJavaStuff\\myJava\\JavaExec\\RequestWords.txt");

    while ((thisString = thisInputFile.readLine()) != null)
    {
	   theseWords.add(thisString);
       requestWordRows++;
    }

    closeTextInPut(thisInputFile);

    return theseWords;

  } // getRequestWords

  public String getWordMatrix ()
                throws IOException, Exception

  {

	String thisString;
	String returnString = "";

    thisInputFile = openTextInPut ("U:\\myJavaStuff\\myJava\\JavaExec\\WordMatrix.txt");

    while ((thisString = thisInputFile.readLine()) != null)
          {returnString = thisString;}

    puzzleRows = Integer.parseInt(Character.toString(returnString.charAt(0))) -1;
    puzzleCols = Integer.parseInt(Character.toString(returnString.charAt(2))) -1;

    closeTextInPut(thisInputFile);

    return returnString;

  } // getWordMatrix

  public void goNorthWest ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordRow++;
	      puzzleWordCol--;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goNorthWest

  public void goNorthEast ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordRow++;
	      puzzleWordCol++;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goNorthEast

  public void goNorth ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordRow--;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goNorth

  public void goSouthEast ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      System.out.println("Char found " + charFound);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordCol++;
	      puzzleWordRow++;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goSouthEast

  public void goSouthWest ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordRow--;
	      puzzleWordCol--;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goSouthWest

  public void goSouth ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordRow++;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goSouth

  public void goWest ()

  {

  continueOK = true;

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
	      charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
			 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordCol--;
		   }
        else continueOK = false;

     } while (continueOK == true);

  } // goWest

  public void goEast ()

  {

  startPos = requestWordsString + ": " + puzzleWordRow.toString() + ":" + puzzleWordCol.toString();

  continueOK = true;

  do {

       char thisChar = requestWordsString.charAt(requestWordCol);

       if (thisChar == puzzleChars[puzzleWordRow][puzzleWordCol])
	      {
          charFound = charFound + Character.toString(thisChar);
	      if (charFound.equals(requestWordsString))
	         {
			 searchWordFoundOK = true;
			 continueOK        = false;
			 isBreakOK         = true;
     		 System.out.println(requestWordsString + " " + startPos + " " + puzzleWordRow + ":" + puzzleWordCol);
		     }
	      requestWordCol++;
	      puzzleWordCol++;
          }
        else
          continueOK = false;

     } while (continueOK == true);

  } // goEast

  public void resetIndicators (Integer x1,
                               Integer x2,
                               Integer x3,
                               Integer x4,
                               String  requestWordsString)

  {

  puzzleWordCol  = x4;
  puzzleWordRow  = x3;
  requestWordCol = x2;
  requestWordRow = x1;
  charFound      = "";
  startPos       = "";
  stopPos        = "";

  } // resetIndicators

  public void findWord (Integer  x1,
                        Integer  x2,
                        Integer  x3,
                        Integer  x4,
                        String   requestWordsString,
                        char     requestWordsChar,
                        char     thisPuzzleChar)

  {

     searchWordFoundOK = false;

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     System.out.println("pwc: " + puzzleWordCol + "; pwr: " + puzzleWordRow + "; clic: " + charsLeftInCol + "; clir: " + charsLeftInRow + "; rwc: " + requestWordsChar + "; rwl: " + requestWordLength);

     charsLeftInRow = Math.abs(puzzleWordRow - puzzleRows);
     charsLeftInCol = Math.abs(puzzleWordCol - puzzleCols);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  <= charsLeftInCol) &&
            (charsLeftInCol >= (requestWordLength -1)))
           goEast();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  >= charsLeftInCol) &&
            (charsLeftInCol <= (requestWordLength -1)))
           goWest();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        {
        if ((puzzleWordCol  >= charsLeftInCol) &&
            (charsLeftInCol <= (requestWordLength -1)) &&
            (puzzleWordRow  >= charsLeftInRow))
           goSouthWest();
	   }

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  <= charsLeftInCol) &&
            (charsLeftInRow >= (requestWordLength -1)) &&
            (puzzleWordRow  <= charsLeftInRow))
           goSouth();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((requestWordLength <= charsLeftInRow) &&
            ((puzzleWordCol +1)   <=  requestWordLength))
           goSouthEast();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  >= charsLeftInCol) &&
            (charsLeftInRow <= (requestWordLength -1)) &&
            (puzzleWordRow  >= charsLeftInRow))
           goNorth();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  <= charsLeftInCol) &&
            (charsLeftInRow >= (requestWordLength -1)) &&
            (puzzleWordRow  <= charsLeftInRow))
           goNorthEast();

     resetIndicators (x1, x2, x3, x4, requestWordsString);

     if (searchWordFoundOK == false)
        if ((puzzleWordCol  >= charsLeftInCol) &&
            (charsLeftInRow >= (requestWordLength -1)) &&
            (puzzleWordRow  <= charsLeftInRow))
           goNorthWest();

  } // findWord

  public void scanPuzzle ()

  {

    for (int x1 = 0; x1 < requestWordRows; x1++)
        {

        isBreakOK          = false;
        requestWordsString = requestWords.get(x1);
        requestWordLength  = requestWordsString.length();

        for (int x2 =0; x2 < requestWordsString.length(); x2++)
            {
			requestWordsChar = requestWordsString.charAt(x2);

            for (int x3 = 0; x3 <= puzzleRows; x3++)
                {
                for (int x4 = 0; x4 <= puzzleCols; x4++)
                    {
                     if ((requestWordsChar == puzzleChars[x3][x4]) &
                         (x2 == 0))
                        {
					     findWord(x1,x2,x3,x4, requestWordsString, requestWordsChar, puzzleChars[x3][x4]);
					     if (isBreakOK)
					         break;
                        }
                        if (isBreakOK)
					       break;
                    }
                    if (isBreakOK)
					   break;
		        }
		        if (isBreakOK)
				   break;
	        }
        }
  } // scanPuzzle

  public static void main(String[] args) throws IOException, Exception
  {

    WordSearch wordSearch = new WordSearch();

    wordMatrix   = wordSearch.getWordMatrix();
    requestWords = wordSearch.getRequestWords();
    puzzleChars  = wordSearch.getPuzzleWords();

    wordSearch.scanPuzzle();

  } // main

} // WordSearch

